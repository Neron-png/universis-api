import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import {ValidationResult} from "../server/errors";
import {TraceUtils} from "@themost/common/utils";
const executeInTransaction = TestUtils.executeInTransaction;
describe('RequestDocumentAction', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        // re-enable development environment
        process.env.NODE_ENV = 'development';
        return done();
    });
    afterEach((done) => {
        delete context.user;
        return done();
    });

    it('should add action as student', async () => {
     //   await executeInTransaction(context, async ()=> {
            // get an active declare action
            let action = await context.model('StudentDeclareAction')
                .where('actionStatus/alternateName').equal('ActiveActionStatus')
                .expand( 'object')
                .silent()
                .getTypedItem();
            expect(action).toBeTruthy();

            const student =  action.object;
            // get related graduation request
            const request = await context.model('GraduationRequestAction')
                .where('id').equal(action.initiator).expand(
                    {
                        name: 'graduationEvent',
                        options:
                            {
                                $expand: 'reportTemplates($select=id)'
                            }
                    }
                ).silent()
                .getItem();

            const reportTemplates = request.graduationEvent.reportTemplates.map(x=>{
                return x.id;
            });

            if (reportTemplates && reportTemplates.length>0) {
                const documentConfigurations = await context.model('DocumentConfiguration').where('id').in(reportTemplates).silent().getTypedItems();
                if (documentConfigurations && documentConfigurations.length > 0) {
                    let actions = [];
                    // add action
                    let forEachConfig = (document) => {
                        try {
                            return new Promise((resolve) => {
                                const documentConfiguration = context.model('DocumentConfiguration').convert(document);
                                documentConfiguration.validate(function (err, result) {
                                    if (err) {
                                        if (err instanceof ValidationResult) {
                                            documentConfiguration.validationResult = err;
                                        } else {
                                            TraceUtils.error(err);
                                            documentConfiguration.validationResult = new ValidationResult(false, 'EFAIL', context.__('An internal server error occurred.'), err.message);
                                        }
                                    }
                                    document.validationResult = result;
                                    return resolve();
                                });
                            });

                        } catch (err) {
                            TraceUtils.error(err);
                        }
                    };
                    for (let i = 0; i < documentConfigurations.length; i++) {
                        const documentConfiguration = documentConfigurations[i];
                        await forEachConfig(Object.assign(documentConfiguration, {
                            student
                        }));
                        if (documentConfiguration.validationResult.success === true) {
                            let newAction = {
                                student: student,
                                object: documentConfiguration,
                                actionStatus: {
                                    alternateName: 'ActiveActionStatus'
                                },
                                initiator: request.id,
                                owner: student.user
                            };
                            actions.push(newAction);
                        }
                    }
                    if (actions.length) {
                        await context.model('RequestDocumentAction').silent().save(actions);
                    }


                }
            }

      //  });
    });





});
