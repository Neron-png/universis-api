import faker from 'faker';
const city = faker.address.city();
const INSTITUTE = {
    id: 20000,
    name: `University of ${city}`,
    alternateName: `${city.substr(0,1).toUpperCase()}U`,
    instituteType: {
        alternateName: 'university'
    },
    local: true,
    instituteConfiguration: {
        useDigitalSignature: false,
        showPendingGrades: false
    }
}

export {
    INSTITUTE
};
