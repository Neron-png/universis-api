import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {Student|any} student
 * @property {Event|any} event
 * @property {number} coefficient
 * @property {number} id
 * @property {string} additionalType
 * @property {string} alternateName
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User|any} createdBy
 * @property {User|any} modifiedBy
 * @augments {DataObject}
 */
@EdmMapping.entityType('EducationEventAttendance')
class EducationEventAttendance extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = EducationEventAttendance;