import _ from 'lodash';
/**
 * @class
 * @constructor
 */
class StudentMapper {
    /**
     * @param {Function} callback
     */
    studentOf(callback) {
        const self = this;
        self.attrOf('student', function(err, result) {
            if (err) { return callback(err); }
            if (_.isNil(result)) {
                //get current student
                self.getModel().resolveMethod('student',[], function(err, result) {
                    if (err) { return callback(err); }
                    self['student'] = result;
                    callback(null, result);
                });
            }
            else {
                callback(null, result);
            }
        });
    }
}

module.exports = StudentMapper;