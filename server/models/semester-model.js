import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @property {number} identifier
 * @property {string} additionalType
 * @property {string} alternateName
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User|any} createdBy
 * @property {User|any} modifiedBy
 */
@EdmMapping.entityType('Semester')
class Semester extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
    /**
     * Returns a collection of courseTypes
     * @param {DefaultDataContext} context
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('All', EdmType.CollectionOf('Semester'))
    static async getAllSemesters(context) {
        const semesters  = await context.model('Semester').asQueryable().getAllItems();
        const semester = semesters.find(x=>{
            return x.id === -1;
        });
        if (!semester) {
            semesters.unshift({"id": -1, "name": context.__("All semesters")});
        }
        return semesters;
    }
}
module.exports = Semester;
