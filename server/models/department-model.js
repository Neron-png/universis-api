import {EdmMapping,EdmType} from '@themost/data/odata';
import _ from 'lodash';
import {DataObject} from "@themost/data/data-object";
import {DataNotFoundError} from "@themost/common/errors";
import {TraceUtils} from "@themost/common/utils";

@EdmMapping.entityType("Department")
/**
 * @class
 * @augments DataObject
 */
class Department extends DataObject {

    /**
     * @constructor
     */
    constructor() {
        super();
    }

    @EdmMapping.func("ActivePrograms", EdmType.CollectionOf("StudyProgram"))
    getActivePrograms() {
        return this.context.model('StudyProgram').where('department').equal(this.id).prepare();
    }

    /**
     * @param {number} year
     * @param {number} period
     * @returns {DataQueryable}
     */
    @EdmMapping.param('period', EdmType.EdmInt32, false)
    @EdmMapping.param('year', EdmType.EdmInt32, false)
    @EdmMapping.func("CourseClasses", EdmType.CollectionOf("CourseClass"))
    getCourseClasses(year, period) {
        return this.context.model('CourseClass')
            .where('course/department').equal(this.getId())
            .and('year').equal(year)
            .and('period').equal(period)
            .prepare();
    }

    @EdmMapping.func("CurrentClasses", EdmType.CollectionOf("CourseClass"))
    getCurrentClasses() {
        const self = this;
        return self.getModel().where('id').equal(self.id).select('currentYear', 'currentPeriod').getItem().then(function (result) {
            if (_.isNil(result)) {
                return Promise.reject(new DataNotFoundError("Department not found"));
            }
            return self.context.model('CourseClass')
                .where('course/department').equal(self.id)
                .and('year').equal(result.currentYear)
                .and('period').equal(result.currentPeriod)
                .prepare();
        });

    }

    /**
     * @returns {DepartmentSnapshot}
     */
    @EdmMapping.param("data", "Object", false, true)
    @EdmMapping.action('snapshot', 'DepartmentSnapshot')
    async createSnapshot(data) {
        /**
         * get LocalDepartment
         * @type {LocalDepartment}
         */
        let department = await this.context.model('LocalDepartment')
            .asQueryable()
            .expand('reportVariables', 'locales', 'organization')
            .where('id').equal(this.getId()).silent().getItem();

        // assign data to department
        department = Object.assign(department, data);
        // set department
        department.department = department.id;
        // set institute attributes name,alternateName, instituteType
        department.instituteName = department.instituteName || department.organization.name;
        department.instituteAlternateName = department.instituteAlternateName || department.organization.alternateName
        department.instituteType = department.instituteType || department.organization.instituteType;

        // remove attributes
        delete department.departmentConfiguration;
        delete department.id;

        // assign report variables
        if (department.reportVariables) {
            department.reportVariables = department.reportVariables.map(department => {
                delete department.id;
                return department;
            });
        }
        // assign locales
        if (department.locales) {
            department.locales = department.locales.map(locale => {
                delete locale.id;
                locale.instituteName = department.instituteName;
                locale.instituteAlternateName = department.instituteAlternateName;
                return locale;
            });
        }
        // return new snapshot
        return await this.context.model('DepartmentSnapshot').save(department);
    }

    /**
     * Updates scholarship rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Student'), false, true)
    @EdmMapping.action('validateStudents', 'GraduationRuleValidateAction')
    async validateStudents(items) {
        try {
            // check all students
            const action = await this.context.model('GraduationRuleValidateAction').silent().save(
                {
                    department: this.getId(),
                    totalStudents: items.length
                });
            await this.checkRules(this.context, this.getId(), items, action);
            return action;
        } catch (err) {
            throw (err);
        }
    }


    checkRules(appContext, department, students, action) {
        const app = appContext.getApplication();
        const context = app.createContext();
        context.user = appContext.user;
        (async function () {
            // check rules for each student
            for (let i = 0; i < students.length; i++) {
                let student = context.model('Student').convert(students[i].id);
                try {
                    await new Promise((resolve, reject) => {
                        return student.checkGraduationRules(null).then(validationResult => {
                            const result = {
                                action: action.id,
                                student: student.id
                            };
                            if (validationResult && validationResult.finalResult) {
                                result.result = validationResult.finalResult.success;
                                result.grade = validationResult.finalResult.data && validationResult.finalResult.data.finalGrade;
                            } else {
                                result.result = false;
                                TraceUtils.error('Could not validate student graduation rules');
                            }
                            return context.model('GraduationRuleValidateResult').silent().save(result).then(res => {
                                return resolve();
                            })
                        }).catch(err => {
                            return resolve();
                        });
                    });
                } catch (err) {
                    TraceUtils.error(err);
                }
            }
        })().then(() => {
            context.finalize(() => {
                // build result

                // after finishing sending mails, an instructor message is created to inform instructor
                action.actionStatus = {alternateName: 'CompletedActionStatus'};
                action.endTime = new Date();
                return context.model('GraduationRuleValidateAction').silent().save(action).then(action => {
                }).catch(err => {
                    TraceUtils.error(err);
                });
            });
        }).catch(err => {
            context.finalize(() => {
                action.actionStatus = {alternateName: 'FailedActionStatus'};
                action.endTime = new Date();
                action.description = err.message;
                return context.model('GraduationRuleValidateAction').silent().save(action).then(action => {
                }).catch(err => {
                    TraceUtils.error(`An error occurred while checking graduation rules for department with id ${department}`);
                    TraceUtils.error(err);
                });
            });
        });
    }
}

module.exports = Department;
