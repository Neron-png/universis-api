import {assert} from 'chai';
import app from '../app';
import Student from '../models/student-model';
import {ValidationResult} from "../errors";
import {TraceUtils} from "@themost/common";


describe('test request rules', () => {

    /**
     * @type ExpressDataContext
     */
    let context;
    before(done => {
        /**
         * @type ExpressDataApplication
         */
        const _app = app.get('ExpressDataApplication');
        // create context
        context = _app.createContext();
        // set current student
        // user with success: 'user_20100248@example.com'
        // user with success and complex rules: 'user_20131066@example.com'
        // user failure: 'user_20138601@example.com'
        context.user = {
            name: 'user_20138601@example.com'
        };
        return done();
    });
    after(done => {
        if (context == null) {
            return done();
        }
        context.finalize(() => {
            return done();
        });
    });

    it('should validate request rules', async () => {
        /**
         * @type Student
         */
        const student = await Student.getMe(context).getTypedItem();
        const documentConfigurations = await context.model('DocumentConfiguration').silent().getTypedItems();

        let forEachDocument = (documentConfiguration) => {
            try {
                return new Promise((resolve) => {
                    documentConfiguration.validate(function (err, result) {
                        if (err) {
                            if (err instanceof ValidationResult) {
                                documentConfiguration.validationResult = err;
                            } else {
                                TraceUtils.error(err);
                                documentConfiguration.validationResult = new ValidationResult(false, 'EFAIL', context.__('An internal server error occurred.'), err.message);
                            }
                        }
                        documentConfiguration.validationResult = result;
                        return resolve();
                    });
                });
            } catch (err) {
                TraceUtils.error(err);
            }
        };

        for (let i = 0; i < documentConfigurations.length; i++) {
            const documentConfiguration = documentConfigurations[i];
            await forEachDocument(Object.assign(documentConfiguration, {
                student
            }));
        }
        // delete student attribute
        documentConfigurations.forEach( (documentConfiguration) => delete documentConfiguration.student );
        console.log(JSON.stringify(documentConfigurations, null, 4));
    });
});
