import {assert} from 'chai';
import app from '../app';
import path from 'path';
import util from 'util';
import {PrivateContentService} from '../services/content-service';
import {ExpressDataApplication} from '@themost/express';
import fs from 'fs';
import mkdirp from 'mkdirp';
import {TestUtils} from '../utils';
import {Base26Number} from '@themost/common';
import rimraf from 'rimraf';
const mkdirpAsync = util.promisify(mkdirp);
const writeFileAsync = util.promisify(fs.writeFile);

describe('PrivateContentService', () => {

    const TEST_CONTENT_ROOT = path.resolve(__dirname, '.tmp/content/private');
    /**
     * @type {ExpressDataApplication}
     */
    let application;
    before(done => {
        application = app.get(ExpressDataApplication.name);
        return done();
    });

    it('should have content service', async () => {
        /**
         * @type {PrivateContentService}
         */
        const privateContentService = application.getService(PrivateContentService);
        assert.isDefined(privateContentService);
    });

    it('should use default root', async () => {
        // use content service
        application.useService(PrivateContentService);
        /**
         * @type {PrivateContentService}
         */
        const privateContentService = application.getService(PrivateContentService);
        assert.isDefined(privateContentService);
        assert.equal(privateContentService.root, path.resolve('content/private'));
    });

    it('should use custom root', async () => {
        // change configuration
        application.getConfiguration().setSourceAt('settings/universis/content/root', TEST_CONTENT_ROOT);
        // use content service
        application.useService(PrivateContentService);
        /**
         * @type {PrivateContentService}
         */
        const privateContentService = application.getService(PrivateContentService);
        assert.isDefined(privateContentService);
        assert.notEqual(privateContentService.root, path.resolve('content/private'));
        assert.equal(privateContentService.root, TEST_CONTENT_ROOT)
    });

    it('should save to custom root', async () => {
        // change configuration
        application.getConfiguration().setSourceAt('settings/universis/content/root', TEST_CONTENT_ROOT);
        // use content service
        application.useService(PrivateContentService);
        /**
         * @type {PrivateContentService}
         */
        const privateContentService = application.getService(PrivateContentService);
        assert.isDefined(privateContentService);
        // copy file to storage
        const context  = application.createContext();
        const unattendedAsync = util.promisify(context.unattended).bind(context);
        await TestUtils.executeInTransaction(context, async () => {
            // create temp folder
            await mkdirpAsync(TEST_CONTENT_ROOT);
            // create temp file
            let tmpFile = path.resolve(__dirname, '.tmp/hello-world.json');
            await writeFileAsync(path.resolve(__dirname, '.tmp/hello-world.json'), JSON.stringify({ message: 'Hello World' }) , 'utf8');

            // create file attributes
            const attrs = {
                contentType: 'application/json',
                name: 'hello-world.json'
            };
            // save file
            await unattendedAsync( callback => {
                // noinspection JSCheckFunctionSignatures
                return privateContentService.copyFrom(context, tmpFile, attrs, function(err) {
                    return callback(err);
                });
            });
            // get id
            assert.isDefined(attrs.id);
            // get file name
            const fileName = Base26Number.toBase26(attrs.id);
            const physicalPath = path.resolve(TEST_CONTENT_ROOT, fileName.substr(0,1), fileName);
            // file exists
            assert.isTrue(fs.existsSync(physicalPath));
            // remove file
            await rimraf.sync(path.resolve(__dirname, '.tmp/hello-world.json'));
            // remove test content
            await rimraf.sync(TEST_CONTENT_ROOT);

        });

    });

});
