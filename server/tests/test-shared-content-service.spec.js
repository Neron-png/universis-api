import {assert} from 'chai';
import app from '../app';
import path from 'path';
import util from 'util';
import {SharedContentService} from '../services/shared-content-service';
import {ExpressDataApplication} from '@themost/express';
import fs from 'fs';
import mkdirp from 'mkdirp';
import {TestUtils} from '../utils';
import {Base26Number} from '@themost/common';
import rimraf from 'rimraf';
const mkdirpAsync = util.promisify(mkdirp);
const writeFileAsync = util.promisify(fs.writeFile);

describe('SharedContentService', () => {

    const TEST_CONTENT_ROOT = path.resolve(__dirname, '.tmp/content/shared');
    /**
     * @type {ExpressDataApplication}
     */
    let application;
    before(done => {
        application = app.get(ExpressDataApplication.name);
        application.useService(SharedContentService);
        return done();
    });

    it('should have content service', async () => {
        /**
         * @type {SharedContentService}
         */
        const sharedContentService = application.getService(SharedContentService);
        assert.isDefined(sharedContentService);
    });

    it('should use default root', async () => {
        // use content service
        application.useService(SharedContentService);
        /**
         * @type {SharedContentService}
         */
        const sharedContentService = application.getService(SharedContentService);
        assert.isDefined(sharedContentService);
        assert.equal(sharedContentService.root, path.resolve('content/shared'));
    });

    it('should use custom root', async () => {
        // change configuration
        application.getConfiguration().setSourceAt('settings/universis/content/shared', TEST_CONTENT_ROOT);
        // use content service
        application.useService(SharedContentService);
        /**
         * @type {SharedContentService}
         */
        const sharedContentService = application.getService(SharedContentService);
        assert.isDefined(sharedContentService);
        assert.notEqual(sharedContentService.root, path.resolve('content/shared'));
        assert.equal(sharedContentService.root, TEST_CONTENT_ROOT)
    });

});
