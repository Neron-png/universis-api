import {ValidationResult} from "../errors";
import {TraceUtils} from "@themost/common/utils";
import _ from "lodash";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterExecute(event, callback) {
    let data = event['result'];
    // validate data
    if (data == null) {
        return callback();
    }
    // validate query and exit
    if (event.query.$group) {
        return callback();
    }
    if (!(Array.isArray(data) && data.length))
    {
        return callback();
    }

    const context = event.model.context;
    (async function () {
        // get student department
        const student = await context.model('Student').where('user/name').equal(context.user.name).silent().getItem();
        if (student == null) {
            return;
        }
        let forEachConfig = (document) => {
            try {
                return new Promise((resolve) => {
                    const documentConfiguration = context.model('DocumentConfiguration').convert(document);
                    documentConfiguration.validate(function (err, result) {
                        if (err) {
                            if (err instanceof ValidationResult) {
                                documentConfiguration.validationResult = err;
                            } else {
                                TraceUtils.error(err);
                                documentConfiguration.validationResult = new ValidationResult(false, 'EFAIL', context.__('An internal server error occurred.'), err.message);
                            }
                        }
                        document.validationResult = result;
                        return resolve();
                    });
                });

            } catch (err) {
                TraceUtils.error(err);
            }
        };
        for (let i = 0; i < data.length; i++) {
            const documentConfiguration = data[i];
            await forEachConfig(Object.assign(documentConfiguration, {
                student
            }));
        }
        // delete student attribute
        data.forEach((documentConfiguration) => delete documentConfiguration.student);
    })().then(() => {
        // filter only document configuration with success validation result
        data.filter(x => {
            return x.validationResult && x.validationResult.success === true;
        });
        return callback();
    }).catch( err => {
        TraceUtils.error(err);
        return callback();
    });

}

