
function afterExecute(event, callback) {
    (async () => {
        if (event && event.emitter && event.emitter.$expand) {
            const expand = event.emitter.$expand;
            if (Array.isArray(expand)) {
                let findIndex = expand.findIndex((item) => item.name === 'reportVariables');
                if (findIndex >= 0) {
                    const context = event.model.context;
                    const ReportVariableTypes = context.model('ReportVariableType');
                    const variableTypes = await ReportVariableTypes.silent().take(-1).getItems();
                    for (const result of event.result) {
                        if (Array.isArray(result.reportVariables)) {
                            const variables = result.reportVariables.reduce((previousValue, currentValue) => {
                                const currentVariable = ReportVariableTypes.convert(currentValue.reportVariable).getId();
                                const variableType = variableTypes.find(item => item.id === currentVariable);
                                if (variableType != null) {
                                    let value = null;
                                    switch (variableType.typeString) {
                                        case 'Text':
                                            value = currentValue.valueText;
                                            break;
                                        case 'Boolean':
                                            value = currentValue.valueBoolean;
                                            break;
                                        case 'Number':
                                            value = currentValue.valueNumber;
                                            break;
                                        case 'Note':
                                            value = currentValue.valueNote;
                                            break;
                                    }
                                    const inLanguage = currentValue.inLanguage;
                                    if (Object.prototype.hasOwnProperty.call(previousValue, inLanguage) === false) {
                                        Object.defineProperty(previousValue, currentValue.inLanguage, {
                                            configurable: true,
                                            enumerable: true,
                                            writable: true,
                                            value: {}
                                        });
                                    }
                                    Object.defineProperty(previousValue[inLanguage], variableType.alternateName, {
                                        configurable: true,
                                        enumerable: true,
                                        writable: true,
                                        value: value
                                    });
                                }
                                return previousValue;
                            }, {});
                            Object.assign(result, {
                                variables
                            });
                        }
                    }
                }
            }
        }
    })().then(() => {
        return callback();
    }).catch((err) => {
       return callback(err);
    });
}

export {
    afterExecute
}
