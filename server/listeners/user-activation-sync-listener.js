import _ from 'lodash';
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    try {
        const context = event.model.context;
        if (event.state===2) {
            //get student person
            event.model.where('id').equal(event.target.id).select('uniqueIdentifier', 'email').silent().first(function(err, userActivation) {
                if (err) { return callback(err); }
                if (_.isNil(userActivation)) {
                    return callback(new Error('User activation attributes cannot be found.'));
                }
                if (_.isNil(userActivation.uniqueIdentifier)) {
                    return callback();
                }
                context.model('Student').where('uniqueIdentifier').equal(userActivation.uniqueIdentifier).select('person').flatten().silent().first(function(err, student) {
                    if (err) { return callback(err); }
                    if (_.isNil(student)) {
                        return callback();
                    }
                    context.model('Person').save({ id:student.person, email:userActivation.email }, function(err) {
                        callback(err);
                    });
                });
            });
        }
        else {
            callback();
        }
    }
    catch (e) {
        callback(e)
    }
}