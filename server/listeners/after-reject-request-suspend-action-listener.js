import { DataError, DataNotFoundError } from '@themost/common';
import { DataObjectState } from '@themost/data';
import ActionStatusType from '../models/action-status-type-model';

export function afterSave(event, callback) {
    (async () => {
        // on request suspend action update
        if (event.state === DataObjectState.Update) {
            const context = event.model.context;
            const previous = event.previous;
            const target = event.target;
            if (previous == null) {
                throw new DataError('E_STATE', 'The previous state of the action cannot be determined', null, 'RequestSuspendAction')
            }
            // if there is a status change from active to cancelled (e.g. Action rejection)
            if ((previous.actionStatus && previous.actionStatus.alternateName === ActionStatusType.ActiveActionStatus)
                && (target.actionStatus && target.actionStatus.alternateName === ActionStatusType.CancelledActionStatus)) {
                    // find relevant StudentSuspendAction by initiator
                    const studentSuspendAction = await context.model('StudentSuspendAction')
                        .where('initiator').equal(target.id)
                        .select('id', 'actionStatus/alternateName as status')
                        .getItem(); 
                    if (studentSuspendAction == null) {
                        throw new DataNotFoundError(`The relevant StudentSuspendAction for RequestSuspendAction ${target.id}
                            cannot be found or is inaccessible.`);
                    }
                    // if the action is active
                    if (studentSuspendAction.status === ActionStatusType.ActiveActionStatus) {
                        // ensure that it is cancelled
                        await context.model('StudentSuspendAction').save({
                            id: studentSuspendAction.id,
                            actionStatus: {
                                alternateName: ActionStatusType.CancelledActionStatus
                            }
                        });
                    }
                    // exit
                    return;
                }
        }
        // exit
        return;
    })().then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
