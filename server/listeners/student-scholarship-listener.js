import {DataConflictError} from "../errors";
import _ from "lodash";

/**
 * @async
 * @param {DataEventArgs} event
 */
export async function beforeSaveAsync(event) {
    const target = event.model.convert(event.target);
    const context = event.model.context;
    const student = _.isObject(event.target.student) ? event.target.student.id : event.target.student;
    // validate scholarship rules
    /**
     * @type {Scholarship}
     */
    const scholarship = context.model('Scholarship').convert(target.scholarship);
    const validationResult = await scholarship.validate(student);
    if (validationResult && validationResult.finalResult) {
        if (validationResult.finalResult.success === true) {
            // update final grade
            event.target.grade = validationResult.finalResult.data && validationResult.finalResult.data.finalGrade;
        }
        else {
            throw new DataConflictError(context.__('Scholarship rules are not met'));
        }
    }
    else {
        throw new DataConflictError(context.__('Scholarship rules are not met'));
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    if (event.state === 4) {
        return callback();
    }
    // execute async method
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}


/**
 * @async
 * @param {DataEventArgs} event
 */
export async function afterSaveAsync(event) {
    if (event.state === 2) {
        return;
    }
    try {
        const context = event.model.context;
        const scholarship = context.model('Scholarship').convert(event.target.scholarship);
        await scholarship.updateTotalStudents();
    } catch (e) {
        throw (e);
    }
}


/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    if (event.state === 4) {
        return callback();
    }
    // execute async method
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}


/**
 * @async
 * @param {DataEventArgs} event
 */
export async function afterRemoveAsync(event) {
    try {
        const context = event.model.context;
        const previous = event.previous;
        const scholarship = context.model('Scholarship').convert(previous.scholarship);
        await scholarship.updateTotalStudents();
    } catch (e) {
        throw (e);
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterRemove(event, callback) {
    return afterRemoveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

