import {PrivateContentService} from "../services/content-service";
import path from "path";
import {HttpNotFoundError} from "@themost/common";
import {csvPostParser} from "../middlewares/csv";
import {xlsPostParser, XlsxContentType} from "../middlewares/xls";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    /**
     * @type *
     */
    // set object
    if (event.target.courseExam) {
        event.target.object = event.target.courseExam;
    }
    if (event.state !== 1) {
        return callback();
    }
    // add file as attachment to action
    let context = event.model.context;
    const file = event.target.file;
    if (!file) {
        return callback();
    }
    // add attachment
    /**
     * get private content service
     * @type {PrivateContentService}
     */
    let service = context.getApplication().getService(PrivateContentService);
    //se attachment attributes from multer file
    let attachment = {
        contentType: file.mimetype,
        name: file.originalname
    };
    // add attachment (in unattended mode)
    context.unattended((cb) => {
        service.copyFrom(context, path.resolve(file.destination, file.filename), attachment, (err) => {
            try {
                if (err) {
                    return cb(err);
                }
                let target = event.model.convert(event.target);
                // add attachment to document action
                return target.property('attachments').insert(attachment).then(() => {
                    return cb();
                }).catch(err => {
                    return cb(err);
                });
            } catch (err) {
                return cb(err);
            }
        });

    }, (err) => {
        return callback(err);
    });
}




/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    // execute async method
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

async function beforeSaveAsync(event) {
    if (event.state !== 2) {
        return;
    }
    // get previous action status
    const previousActionStatus = event.previous && event.previous.actionStatus;
    if (previousActionStatus === null) {
        throw new Error('Previous status cannot be empty at this context.');
    }
    // validate previous status
    if (previousActionStatus.alternateName !== 'PotentialActionStatus') {
        return;
    }
    const actionStatus = event.target.actionStatus;
    if (actionStatus && actionStatus.alternateName !== 'ActiveActionStatus') {
        return;
    }
        const context = event.model.context;
    const courseExam = await context.model('CourseExam').where('id').equal(event.target.object).silent().getTypedItem();
        let target = event.model.convert(event.target);
        // get exam document action attachment
        let attachment = await target.property('attachments').silent().getItem();
        if (typeof attachment === 'undefined') {
            throw new HttpNotFoundError('Document action attachment is missing.');
        }
        const body = await parseFile(event.model.context, attachment);
        if (body) {
            /**
             * get student export view
             * @type {DataModelView}
             */
            let view = context.model('CourseExamStudentGrade').getDataView('export');
            // set course exam grades
            courseExam.grades = view.fromLocaleArray(body);
            courseExam.status = {alternateName :'completed'};
            courseExam.completedByUser = {alternateName:  context.user.userName};
            // save course exam
        return await new Promise((resolve, reject) => {
            context.unattended((cb)=> {
                courseExam.save(context, (err) => {
                    return cb(err);
                });
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                if (courseExam.validationResult.success === false) {
                // restore documentAction data
                event.target.actionStatus = {
                    "alternateName": "PotentialActionStatus"
                };
                    event.target.object = courseExam;
                    return reject(courseExam.validationResult);
                }
                // set course exam object
                event.target.object = courseExam;
                event.target.courseExam = courseExam;
                //trace grades
                let grades=courseExam.grades.filter(x=>{
                    return x.validationResult && x.validationResult.success === true && x.validationResult.code === 'SUCC';
                });
                // get identifier form courseExamDocument

                return context.model('CourseExamDocument').where('id').equal(courseExam.docId).silent().getItem().then(document=> {
                if (document) {
                    event.target.additionalResult = document.identifier;
                }
                if (grades && grades.length) {
                    //prepare trace grade
                    grades.forEach(grade => {
                        delete grade.id;
                        grade.$state = 1;
                        grade.action = event.target.id;
                        grade.semester = grade.registrationSemester;
                            grade.course=courseExam.course;
                        });
                        //save student grade action trace
                        return context.model('StudentGradeActionTrace').silent().save(grades).then(traces => {
                            return resolve();
                        }).catch(err => {
                            return reject(err);
                    });
                }
                    //get course exam result document
                    if (event.target.additionalResult) {
                        event.target.additionalResult = document;
            }
                    return resolve();
                });
            });
        });
        }
    throw new Error('Source content type is not yet implemented');
}

function readStream(stream) {
    return new Promise((resolve, reject) => {
        let buffers = [];
        stream.on('data', (d) => {
            buffers.push(d);
        });
        stream.on('end', () => {
            return resolve(Buffer.concat(buffers));
        });
        stream.on('error', (err) => {
            return reject(err);
        });
    });
}

async function parseFile (context, attachment)
{
    /**
     * get private content service
     * @type {PrivateContentService}
     */
    let service = context.getApplication().getService(PrivateContentService);
    /**
     * get attachment readable stream
     * @type {ReadableStream}
     */
    let stream = await new Promise((resolve, reject) => {
        context.unattended((cb)=> {
            service.createReadStream(context, attachment, (err, result) => {
                return cb(err, result);
            });
        }, (err, result) => {
            if (err) {
                return reject(err);
            }
            return resolve(result);
        });
    });
    let body;
    let buffer = await readStream(stream);
    // parse text/csv
    if (attachment.contentType === 'text/csv' || attachment.contentType === 'application/csv' ||
        ((attachment.contentType === 'application/vnd.ms-excel' || attachment.contentType === 'application/octet-stream')
            && (/\.(csv)$/i).test(attachment.name))) {
        body = await new Promise((resolve, reject) => {
            // create a request like object
            let req = {
                file: {
                    buffer: buffer,
                    mimetype: "text/csv"
                }
            };
            // call csvPostHandler to parse csv
            csvPostParser({
                name: "file"
            })(req, null, (err) => {
                if (err) {
                    return reject(err);
                }
                // get body
                return resolve(req.body);
            });
        });
    }
    // parse application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
    else if (attachment.contentType === XlsxContentType || ((attachment.contentType === 'application/octet-stream')
        && (((/\.(xlsx)$/i).test(attachment.name)) || (/\.(xls)$/i).test(attachment.name)))) {
        body = await new Promise((resolve, reject) => {
            // create a request like object
            let req = {
                file: {
                    buffer: buffer,
                    mimetype: XlsxContentType
                }
            };
            // call csvPostHandler to parse csv
            xlsPostParser({
                name: "file"
            })(req, null, (err) => {
                if (err) {
                   return reject(err);
                }
                // get body
                return resolve(req.body);
            });
        });
    }
    return body;
}
