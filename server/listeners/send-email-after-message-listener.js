import { ApplicationServiceAsListener } from "../services/application-service-as-listener";
import { getMailer } from '@themost/mailer';
import { Args, TraceUtils } from '@themost/common';
import moment from 'moment';

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    const context = event.model.context;
    if (event.state !== 1) {
        return;
    }
    const mailTemplate = await context.model('MailConfiguration').where('target').equal('SendEmailAfterMessage').silent().getItem();
    if (mailTemplate == null) {
        return;
    }
    const student = await context.model('Student').find(event.target.student).expand('person').silent().getItem();
    Args.notNull(student, 'Student');
    Args.notNull(student.person, 'Recipient');
    //get target object
    const target = await event.model.where('id').equal(event.target.id).expand('sender').getItem();

    const mailer = getMailer(context);

    const data = {
        'student': student,
        'model': target
    };

    // check if student's email exists
    if (student.person.email == null || (student.person.email && typeof student.person.email !== 'string')) {
        TraceUtils.warn(`Cannot send email for ${student.person.familyName} ${student.person.givenName}. Student [${student.id}] email does not exist.`);
        return;
    }

    return await new Promise((resolve, reject) => {
        mailer.to(student.person.email)
            .subject(event.target.subject)
            .template(mailTemplate.template)
            .send(Object.assign(data, {
                html: {
                    moment: moment,
                    context: context
                }
            }), (err) => {
                if (err) {
                    try {
                        TraceUtils.error('An error occurred while trying to send an email notification after student message.');
                        TraceUtils.error(`student=${student.person.email}, template=${mailTemplate.template}`)
                        TraceUtils.error(err);
                    } catch (err1) {
                        // do nothing
                    }
                    return resolve();
                }
                return resolve();
            });
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {

    afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        TraceUtils.error('SendEmailAfterMessageListener.afterSave()');
        TraceUtils.error(err);
        return callback();
    });
}

export class SendEmailAfterMessage extends ApplicationServiceAsListener {
    constructor(app) {
        super(app);
        // install this listener as member of event listeners of StudentMessage model
        this.install('StudentMessage', __filename);

        // import default mail configuration
        (async function () {
            const context = app.createContext();
            const mailConfiguration = await context.model('MailConfiguration').where('target').equal('SendEmailAfterMessage').silent().getItem();
            if (mailConfiguration == null) {
                await context.model('MailConfiguration').silent().save({
                    "target": "SendEmailAfterMessage",
                    "status": 1,
                    "previouStatus": 1,
                    "subject": "Send email after in-message",
                    "template": "mail-after-message",
                    "state": 1
                });
            }
            context.finalize();
        })().then(() => {
            //
        }).catch(err => {
            TraceUtils.error(err);
        });
    }
}