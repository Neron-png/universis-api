import { ApplicationService } from '@themost/common';
import { ODataModelBuilder } from '@themost/data';
import { serviceRouter } from '@themost/express';
import express from 'express';

class SetDefaultTopQueryParamService extends ApplicationService {
    constructor(app) {
        super(app);
        if (app.container) {
            // subscribe and wait for container
            app.container.subscribe((container) => {
                if (container == null) {
                    return;
                }
                const router = serviceRouter;
                const addRoute = express.Router();

                function setDefaultTopOptionForAny(req, res, next) {
                    if (Object.prototype.hasOwnProperty.call(req.query, '$top') === true) {
                        return next();
                    }
                    Object.assign(req.query, {
                        $top: -1
                    });
                    return next();
               }
                addRoute.get('/CourseTypes', setDefaultTopOptionForAny);
                addRoute.get('/CourseAreas', setDefaultTopOptionForAny);
                addRoute.get('/:entitySet', function setDefaultTopOption(req, res, next) {
                    if (Object.prototype.hasOwnProperty.call(req.query, '$top') === true) {
                        return next();
                    }
                    const builder = req.context.getApplication().getService(ODataModelBuilder);
                    if (builder == null) {
                        return next();
                    }
                    const entitySet = builder.getEntitySet(req.params.entitySet);
                    if (entitySet == null) {
                        return next();
                    }
                    if (entitySet && entitySet.entityType && entitySet.entityType.implements === 'Enumeration') {
                        Object.assign(req.query, {
                            $top: -1
                        });
                    }
                    return next();
               });
               router.stack.unshift(...addRoute.stack);
            });
        }
    }
}

export {
    SetDefaultTopQueryParamService
}