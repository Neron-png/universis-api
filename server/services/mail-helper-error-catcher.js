import {MailHelper} from '@themost/mailer';
import {TraceUtils, ApplicationService} from "@themost/common";

class MailHelperErrorCatcher extends ApplicationService {
    constructor(app) {
        super(app);
        // validate that override has not been already applied
        if (Object.prototype.hasOwnProperty.call(MailHelper.prototype.send, 'MailHelperErrorCatcher')) {
            return;
        }
        // get MailHelper.prototype.send method
        const superSend = MailHelper.prototype.send;
        MailHelper.prototype.send = function(data, callback) {
            const thisArg = this;
            return superSend.bind(thisArg)(data, (err, result) => {
               if (err) {
                   // get user or anonymous
                   const user = (thisArg.context && thisArg.context.user && thisArg.context.user.name) || 'anonymous';
                   // write event
                   return thisArg.context.model('EventLog').silent().save({
                       title: `[${thisArg.options.subject || 'No Subject'}] ${err.message}`,
                       eventType: 2,
                       username: user,
                       eventSource: 'sendmail',
                       eventApplication: 'UNIVERSIS'
                   }).then(() => {
                       // return send error
                       return callback(err);
                   }).catch( error => {
                       TraceUtils.error('An error occurred while trying to write event for mail send failure.');
                       TraceUtils.error(error);
                       // return send error
                       return callback(err);
                   });
               }
               // otherwise return without error
               return callback(null, result);
            });
        };
        // assign property for validating override
        Object.assign(MailHelper.prototype.send, {
            MailHelperErrorCatcher: true
        });
    }
}

export {MailHelperErrorCatcher}
