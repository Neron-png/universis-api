import { ApplicationServiceAsListener } from "./application-service-as-listener";
import { AbstractMethodError} from "@themost/common";

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
function afterSave(event, callback) {
    throw new AbstractMethodError();
}

export class CreateExternalInstructorUserStrategy extends ApplicationServiceAsListener {
	constructor(app, targetModel, targetListener) {
		super(app);
		// install the target listener as member of event listeners of target model
		this.install(targetModel, targetListener);
	}
}
